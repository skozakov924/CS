provider "google" {
  credentials = var.key
  project     = var.project
  region      = var.region
  zone        = var.zone
}