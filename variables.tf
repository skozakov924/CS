variable "key" {
  default = "my-project-sn-320708-a66f867ef6b0.json"
}
variable "project" {
  default = "my-project-sn-320708"
}
variable "service_account" {
  default = "Soulfire"
}
variable "region" {
  default = "europe-west3"
}
variable "zone" {
  default = "europe-west3-c"
}
variable "machine_type" {
  default = "e2-medium"
}
variable "image" {
  default = "ubuntu-2004-lts"
}
variable "instance_name" {
  default = "swarm"
}

variable "private_key_path" {
  description = "Path to file containing private key"
  default     = "Keys/for_GCP"
}

variable "pablic_key_path" {
  description = "Path to file containing public key"
  default     = "Keys/for_GCP.pub"
}



//VPC vars

//variable "ip_cidr_range_private" {
//  default = "10.0.12.0/24"
//}
//variable "public_key_path" {
//  description = "Path to file containing public key"
//  default     = ".ssh/devops095.pub"
//}
//variable "private_key_path" {
//  description = "Path to file containing private key"
//  default     = ".ssh/devops095_ossh.pem"
//}