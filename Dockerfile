FROM node:12 AS base

# создание директории приложения
WORKDIR /usr/src/app

# установка зависимостей
# символ астериск ("*") используется для того чтобы по возможности
# скопировать оба файла: package.json и package-lock.json
COPY package*.json ./
COPY . .
EXPOSE 6000

FROM base AS dev
ENV NODE_ENV=development
RUN npm install
CMD [ "node", "./src/app.js" ]

FROM base AS prod
ENV NODE_ENV=production
RUN npm ci --only=production
CMD [ "node", "./src/app.js" ]


#FROM base AS dependencies
#RUN npm set progress=false && npm config set depth 0
#RUN npm ci --only=production
## copy production node_modules aside
#RUN cp -R node_modules prod_node_modules
## install ALL node_modules, including 'devDependencies'
#RUN npm install

#FROM dependencies AS test
## копируем исходный код
#COPY . .
#RUN  npm run lint && npm run setup && npm run test


#COPY . .
#
#
#CMD [ "node", "./src/app.js" ]
