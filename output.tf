output "public_ip" {
  value = google_compute_instance.swarm.network_interface[0].network_ip
}