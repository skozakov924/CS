#!/bin/bash
apt install apache2 -y
sudo ufw enable

sudo apt update
sudo apt install -y docker.io
sudo docker swarm init
sudo docker network create --driver=overlay traefik-public
