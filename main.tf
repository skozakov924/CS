//resource "google_service_account" "Soulfire" {
//  account_id = "106425725963706746102"
//  display_name = var.service_account
//}
data "template_file" "swarm" {
  template = file("/home/vagrant/SN/CS/scripts/docker.sh")
}

resource "google_compute_instance" "swarm" {
  name = var.instance_name
  machine_type = var.machine_type
  tags = [
    "ssh",
    "swarm"]
  zone = var.zone


  boot_disk {
    initialize_params {
      image = var.image
    }
  }

  metadata_startup_script = data.template_file.swarm.rendered
  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_dns_managed_zone" "stage" {
  dns_name   = "margo.pp.ua."
  name       = "stage"
  visibility = "public"
  dnssec_config {
    state = "off"
  }
}

resource "google_dns_record_set" "A" {
  managed_zone = google_dns_managed_zone.stage.name
  name         = "swarm.${google_dns_managed_zone.stage.dns_name}"
  rrdatas      = [google_compute_instance.swarm.network_interface[0].access_config[0].nat_ip]
  ttl          = 300
  type         = "A"
}






